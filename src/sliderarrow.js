import React from "react";
// import Icon from "./Icon";
import { WifiOutlined } from "@ant-design/icons";

export default ({ className, to, onClick }) => (
  <button
    type="button"
    onClick={onClick}
    className={`button button--text button--icon ${className}`}
    aria-label={to}
  >
    {/* <Icon className="icon" icon={to} /> */}
    <WifiOutlined className="icon" icon={to} />
  </button>
);
