import React from "react";
import { Row, Col, Form, Input, Dropdown, Button, Carousel } from "antd";
import style from "./landingpage.module.css";
import iphone from "./assets/img/i-phone-m.png";
import AwesomeSlider from "react-awesome-slider";
import farouk from "./assets/img/bg4.jpg";
import farouk1 from "./assets/img/bg8.jpg";
import withAutoplay from "react-awesome-slider/dist/autoplay";
import rocket from "./assets/img/chatbot.png";
import "react-awesome-slider/dist/styles.css";
import footer from "./assets/img/footer.png";
import "antd/dist/antd.css";
import facebook from "./assets/img/facebook.png";
import google from "./assets/img/google.png";
import instagram from "./assets/img/instagram.png";
import twitter from "./assets/img/twitter.png";
import googleplay from "./assets/img/playstore.png";
import appstore from "./assets/img/appstore.png";
import youtube from "./assets/img/youtube.png";
import AOS from "aos";
import "slick-carousel/slick/slick.css";

import "slick-carousel/slick/slick-theme.css";
import SliderArrow from "./sliderarrow.js";

import Slider from "react-slick";
import "aos/dist/aos.css"; // You can also use <link> for styles
// ..

AOS.init();

const AutoplaySlider = withAutoplay(AwesomeSlider);

const LandingPage = () => {
  const settings = {
    dots: true,
    arrows: true,

    infinite: true,
    // prevArrow: <SliderArrow to="prev" />,
    // nextArrow: <SliderArrow to="next" />,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  const pictureSlider = [
    {
      key: "1",
      url: farouk,
    },
    {
      key: "2",
      url: farouk1,
    },
    {
      key: "3",
      url: farouk,
    },
  ];

  function onChange(a, b, c) {
    console.log(a, b, c);
  }

  const contentStyle = {
    height: "160px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };

  const chatBotMenu = (
    <div className={style.chatbotMenuWrapper}>
      <Row className={style.chatbotMenu}>
        <Col className={style.chatbotTop}>
          <h3 className={style.h3cb}>Selamat datang di CariParkir</h3>
          <p className={style.pcb}>
            Anda dapat pula menghubungi Call Center CariParkir: 1-500-697
          </p>
        </Col>
      </Row>
      <Row justify="center" className={style.chatbotMenuBottom}>
        <Col lg={4}>
          <div className={style.callcenterWrapper}>
            <img src={rocket} className={style.callcenter}></img>
          </div>
        </Col>
        <Col lg={18}>
          <Form>
            <div className={style.formChat}>
              <p className={style.pcb}>
                Ada yang bisa kami bantu? Sebelumnya silakan isi informasi
                berikut
              </p>

              <div className={style.wrapperChatbot}>
                <Input placeholder="Nama Anda" />
              </div>
              <div className={style.wrapperChatbot}>
                <Input placeholder="Alamat Email Anda" />
              </div>
              <div className={style.wrapperChatbot}>
                <Input placeholder="Nomor HP Anda" />
              </div>
            </div>
            <Button type="primary" className={style.chatbotButton}>
              Sambungkan
            </Button>
          </Form>
        </Col>
      </Row>
    </div>
  );

  return (
    <div>
      <div>
        <AutoplaySlider
          className={style.wrapperSlider}
          play={true}
          cancelOnInteraction={false} // should stop playing on user interaction
          interval={6000}
        >
          <div data-src={farouk1} />
          <div data-src={farouk1} />
          <div data-src={farouk1} />
        </AutoplaySlider>

        <div className={style.chatbotMenuWrapperOuter}>
          <Dropdown
            overlay={chatBotMenu}
            placement="topLeft"
            trigger={["click"]}
          >
            <div
              className={style.chatbotWrapper}
              onClick={(e) => e.preventDefault()}
            >
              <img
                style={{ width: "100%", cursor: "pointer" }}
                src={rocket}
                className={style.chatbot}
              ></img>
            </div>
          </Dropdown>
        </div>
      </div>
      <br /> <br /> <br /> <br />
      {/* Atas */}
      <div className={style.wrapperLandingFirst}>
        <Row justify="center" className={style.rowLandingTop}>
          <Col
            xs={{ span: 10, offset: 5 }}
            sm={{ span: 10, offset: 5 }}
            md={{ span: 10, offset: 7 }}
            lg={{ span: 10, offset: 7 }}
            xl={{ span: 10, offset: 7 }}
          >
            <Row
              gutter={2}
              justify="center"
              className={style.rowLeftTitleContent}
            >
              <Col xs={12} sm={12} md={12} lg={10}>
                <h1>Regular Parking</h1>
              </Col>
              <Col xs={12} sm={12} md={12} lg={14}>
                <div className={style.motocycle}>
                  <h4 className={style.motocycleTitle}>motorcycle</h4>
                  <div style={{ borderBottom: "3px solid #ffd300" }}></div>
                </div>
              </Col>
              <Col lg={24}>
                <p className={style.content}>
                  Tak perlu bingung lupa bawa dompet. Parkir motor (roda dua)
                  mudah dengan cashless di semua lokasi.
                </p>
              </Col>
            </Row>
          </Col>
          <Col
            xs={{ span: 7 }}
            sm={{ span: 7 }}
            md={{ span: 5 }}
            lg={{ span: 5 }}
            xl={{ span: 5 }}
          >
            <div style={{ width: "100%", height: "100%" }}>
              <img src={iphone} style={{ width: "100%", height: "auto" }}></img>
            </div>
          </Col>
        </Row>
        {/* Atas */}
        {/* Bawah */}
        <Row justify="center" className={style.rowLandingBottom}>
          <Col
            xs={{ span: 7 }}
            sm={{ span: 7 }}
            md={{ span: 5 }}
            lg={{ span: 5 }}
            xl={{ span: 5 }}
          >
            <div>
              <img src={iphone} style={{ width: "100%", height: "auto" }}></img>
            </div>
          </Col>
          <Col
            xs={{ span: 10 }}
            sm={{ span: 10 }}
            md={{ span: 10 }}
            lg={{ span: 10 }}
            xl={{ span: 10 }}
          >
            <Row
              gutter={16}
              justify="center"
              className={style.rowLeftTitleContent}
            >
              <Col
                //   xs={{ span: 4, push: 4 }}
                //   sm={{ span: 4, push: 4 }}
                xs={12}
                sm={12}
                md={12}
                lg={14}
              >
                <div className={style.motocycle}>
                  <h4 className={style.motocycleTitle}>motorcycle</h4>
                  <div style={{ borderBottom: "3px solid #ffd300" }}></div>
                </div>
              </Col>
              <Col
                //   xs={{ span: 4, pull: 4 }}
                //   sm={{ span: 4, pull: 4 }}
                xs={12}
                sm={12}
                md={12}
                lg={10}
              >
                <h1 style={{ textAlign: "right" }}>Valet</h1>
              </Col>

              <Col lg={24}>
                <p className={style.contentBottom}>
                  Tak perlu bingung lupa bawa dompet. Parkir motor (roda dua)
                  mudah dengan cashless di semua lokasi.
                </p>
              </Col>
            </Row>
          </Col>
          <Col xs={4} sm={4} md={7} lg={7}></Col>
        </Row>
      </div>
      {/* Bawah */}
      {/* Atas */}
      <div className={style.wrapperLandingSecond}>
        <Row justify="center" className={style.rowLandingTop}>
          <Col
            xs={{ span: 10, offset: 5 }}
            sm={{ span: 10, offset: 5 }}
            md={{ span: 10, offset: 7 }}
            lg={{ span: 10, offset: 7 }}
            xl={{ span: 10, offset: 7 }}
          >
            <Row
              gutter={16}
              justify="center"
              className={style.rowLeftTitleContent}
            >
              <Col xs={12} sm={12} md={12} lg={10}>
                <h1>Regular Parking</h1>
              </Col>
              <Col xs={12} sm={12} md={12} lg={14}>
                <div className={style.motocycle}>
                  <h4 className={style.motocycleTitle}>motorcycle</h4>
                  <div style={{ borderBottom: "3px solid #ffd300" }}></div>
                </div>
              </Col>
              <Col lg={24}>
                <p className={style.content}>
                  Tak perlu bingung lupa bawa dompet. Parkir motor (roda dua)
                  mudah dengan cashless di semua lokasi.
                </p>
              </Col>
            </Row>
          </Col>
          <Col
            xs={{ span: 7 }}
            sm={{ span: 7 }}
            md={{ span: 5 }}
            lg={{ span: 5 }}
            xl={{ span: 5 }}
          >
            <div style={{ width: "100%", height: "100%" }}>
              <img src={iphone} style={{ width: "100%", height: "auto" }}></img>
            </div>
          </Col>
        </Row>
        {/* Atas */}
        {/* Bawah */}
        <Row justify="center" className={style.rowLandingBottom}>
          <Col
            xs={{ span: 7 }}
            sm={{ span: 7 }}
            md={{ span: 5 }}
            lg={{ span: 5 }}
            xl={{ span: 5 }}
          >
            <div>
              <img src={iphone} style={{ width: "100%", height: "auto" }}></img>
            </div>
          </Col>
          <Col
            xs={{ span: 10 }}
            sm={{ span: 10 }}
            md={{ span: 10 }}
            lg={{ span: 10 }}
            xl={{ span: 10 }}
          >
            <Row
              gutter={16}
              justify="center"
              className={style.rowLeftTitleContent}
            >
              <Col
                //   xs={{ span: 4, push: 4 }}
                //   sm={{ span: 4, push: 4 }}
                xs={12}
                sm={12}
                md={12}
                lg={14}
              >
                <div className={style.motocycle}>
                  <h4 className={style.motocycleTitle}>motorcycle</h4>
                  <div style={{ borderBottom: "3px solid #ffd300" }}></div>
                </div>
              </Col>
              <Col
                //   xs={{ span: 4, pull: 4 }}
                //   sm={{ span: 4, pull: 4 }}
                xs={12}
                sm={12}
                md={12}
                lg={10}
              >
                <h1 style={{ textAlign: "right" }}>Valet</h1>
              </Col>

              <Col lg={24}>
                <p className={style.contentBottom}>
                  Tak perlu bingung lupa bawa dompet. Parkir motor (roda dua)
                  mudah dengan cashless di semua lokasi.
                </p>
              </Col>
            </Row>
          </Col>
          <Col xs={4} sm={4} md={7} lg={7}></Col>
        </Row>
      </div>
      {/* Bawah */}
      {/* Atas */}
      <div className={style.wrapperLandingSecond}>
        <Row justify="center" className={style.rowLandingTop}>
          <Col
            xs={{ span: 10, offset: 5 }}
            sm={{ span: 10, offset: 5 }}
            md={{ span: 10, offset: 7 }}
            lg={{ span: 10, offset: 7 }}
            xl={{ span: 10, offset: 7 }}
          >
            <Row
              gutter={16}
              justify="center"
              className={style.rowLeftTitleContent}
            >
              <Col xs={12} sm={12} md={12} lg={10}>
                <h1>Regular Parking</h1>
              </Col>
              <Col xs={12} sm={12} md={12} lg={14}>
                <div className={style.motocycle}>
                  <h4 className={style.motocycleTitle}>motorcycle</h4>
                  <div style={{ borderBottom: "3px solid #ffd300" }}></div>
                </div>
              </Col>
              <Col lg={24}>
                <p className={style.content}>
                  Tak perlu bingung lupa bawa dompet. Parkir motor (roda dua)
                  mudah dengan cashless di semua lokasi.
                </p>
              </Col>
            </Row>
          </Col>
          <Col
            xs={{ span: 7 }}
            sm={{ span: 7 }}
            md={{ span: 5 }}
            lg={{ span: 5 }}
            xl={{ span: 5 }}
          >
            <div style={{ width: "100%", height: "100%" }}>
              <img src={iphone} style={{ width: "100%", height: "auto" }}></img>
            </div>
          </Col>
        </Row>
        {/* Atas */}
        {/* Bawah */}
        <Row justify="center" className={style.rowLandingBottom}>
          <Col
            xs={{ span: 7 }}
            sm={{ span: 7 }}
            md={{ span: 5 }}
            lg={{ span: 5 }}
            xl={{ span: 5 }}
          >
            <div>
              <img src={iphone} style={{ width: "100%", height: "auto" }}></img>
            </div>
          </Col>
          <Col
            xs={{ span: 10 }}
            sm={{ span: 10 }}
            md={{ span: 10 }}
            lg={{ span: 10 }}
            xl={{ span: 10 }}
          >
            <Row
              gutter={16}
              justify="center"
              className={style.rowLeftTitleContent}
            >
              <Col
                //   xs={{ span: 4, push: 4 }}
                //   sm={{ span: 4, push: 4 }}
                xs={12}
                sm={12}
                md={12}
                lg={14}
              >
                <div className={style.motocycle}>
                  <h4 className={style.motocycleTitle}>motorcycle</h4>
                  <div style={{ borderBottom: "3px solid #ffd300" }}></div>
                </div>
              </Col>
              <Col
                //   xs={{ span: 4, pull: 4 }}
                //   sm={{ span: 4, pull: 4 }}
                xs={12}
                sm={12}
                md={12}
                lg={10}
              >
                <h1 style={{ textAlign: "right" }}>Valet</h1>
              </Col>

              <Col lg={24}>
                <p className={style.contentBottom}>
                  Tak perlu bingung lupa bawa dompet. Parkir motor (roda dua)
                  mudah dengan cashless di semua lokasi.
                </p>
              </Col>
            </Row>
          </Col>
          <Col xs={4} sm={4} md={7} lg={7}></Col>
        </Row>
      </div>
      {/* Bawah */}
      {/* Footer */}
      <div className={style.footer}>
        <Row gutter={[0, 24]} justify="center" gutter={24}>
          <Col xs={20} sm={20} md={5} lg={5} xl={5}>
            <div className={style.wrapperImageLogoCP}>
              <img src={footer} style={{ width: "100%" }}></img>
            </div>
          </Col>
          <Col xs={20} sm={20} md={5} lg={5} xl={5}>
            <div className={style.dividerFooter}>
              <a href="/About/about" className={style.footerParagraph}>
                About Us
              </a>
            </div>
            <div className={style.dividerFooter}>
              <a href="/" className={style.footerParagraph}>
                Products
              </a>
            </div>
            <div className={style.dividerFooter}>
              <a href="/Blog/blog" className={style.footerParagraph}>
                Blog
              </a>
            </div>
            <div className={style.dividerFooter}>
              <a href="/BecomePartner" className={style.footerParagraph}>
                Become Our Partner
              </a>
            </div>
          </Col>
          <Col xs={20} sm={20} md={5} lg={5} xl={5}>
            <div className={style.dividerFooter}>
              <a href="/About/about" className={style.footerParagraph}>
                Help Center
              </a>
            </div>
            <div className={style.dividerFooter}>
              <a href="/" className={style.footerParagraph}>
                Contact Us
              </a>
            </div>
            <div className={style.dividerFooter}>
              <a href="/Blog/blog" className={style.footerParagraph}>
                Terms & Condition
              </a>
            </div>
            <div className={style.dividerFooter}>
              <a href="/BecomePartner" className={style.footerParagraph}>
                Privacy Policy
              </a>
            </div>
          </Col>
          <Col xs={20} sm={20} md={5} lg={5} xl={5}>
            <div>
              <h2 className={style.downloadButton}>Download</h2>
            </div>
            <Row gutter={4} className={style.rowFooterGoogleplay}>
              <Col xs={10} sm={10} md={12} lg={12} xl={12}>
                <a href="#">
                  <div className={style.wrapperImageFooter}>
                    <img
                      src={appstore}
                      className={style.imgGoogleplay}
                      style={{ width: "100%" }}
                    ></img>
                  </div>
                </a>
              </Col>
              <Col
                xs={10}
                sm={10}
                md={12}
                lg={12}
                xl={12}
                className={style.colFooterGoogleplay}
              >
                <a href="#">
                  <div className={style.wrapperImageFooter}>
                    <img
                      src={googleplay}
                      className={style.imgGoogleplay}
                      style={{ width: "100%" }}
                    ></img>
                  </div>
                </a>
              </Col>
            </Row>
            <div>
              <h2 className={style.downloadButton}>Follow Us</h2>
            </div>

            <Row>
              <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                <a href="">
                  <div className={style.followUs}>
                    <img src={facebook} style={{ width: "80%" }}></img>
                  </div>
                </a>
              </Col>
              <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                <a href="">
                  <div className={style.followUs}>
                    <img src={twitter} style={{ width: "80%" }}></img>
                  </div>
                </a>
              </Col>
              <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                <a href="">
                  <div className={style.followUs}>
                    <img src={youtube} style={{ width: "80%" }}></img>
                  </div>
                </a>
              </Col>
              <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                <a href="">
                  <div className={style.followUs}>
                    <img src={google} style={{ width: "80%" }}></img>
                  </div>
                </a>
              </Col>
              <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                <a href="">
                  <div className={style.followUs}>
                    <img src={instagram} style={{ width: "80%" }}></img>
                  </div>
                </a>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row justify="center">
          <hr className={style.garisFooter} />
          <h2 className={style.garisFooterParagraph}>
            © 2020 CariParkir.co.id. All Rights Reserved
          </h2>
        </Row>
      </div>
    </div>
  );
};

export default LandingPage;
