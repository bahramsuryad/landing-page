import React, { useState } from "react";
import { Row, Col, Card, Button } from "antd";
import roundedbottom from "./assets/img/oval.png";
import roland from "./assets/img/roland.jpg";
import style from "./product.module.css";
import rocket from "./assets/img/rocket.png";
import oval from "./assets/img/oval-2.png";
import iphone from "./assets/img/i-phone-m.png";
import iphones from "./assets/img/i-phone-s.png";
import ovalactive from "./assets/img/oval3.png";
import click from "./assets/img/click.png";
import iphoney from "./assets/img/i-phone-y.png";
import jakarta from "./assets/img/jakarta.png";
import lockey from "./assets/img/lockey.png";
import "antd/dist/antd.css";

const Product = () => {
  const regularParkingData = [
    {
      type: "column1",
      config: {
        xs: 20,
        sm: 15,
        md: 15,
        lg: 6,
        xl: 6,
      },
      data: [
        {
          type: "card",
          no: 3,
          key: 3,
          ovalimg: oval,
          image: iphone,
          title: "Parkirkan Kendaraan",
          desc: "Scan barcode di lokasi mitra parkir saat Anda ingin keluar",
        },
        {
          type: "card",
          no: 2,
          key: 2,
          ovalimg: oval,
          image: iphoney,
          title: "Parkirkan Kendaraan",
          desc: "Scan barcode di lokasi mitra parkir saat Anda ingin keluar",
        },
      ],
    },
    {
      type: "column2",
      config: {
        xs: 20,
        sm: 15,
        md: 15,
        lg: 6,
        xl: 6,
      },
      data: [
        {
          type: "image-handphone",
        },
        {
          type: "download",
        },
        {
          type: "card",
          no: 1,
          key: 1,
          ovalimg: oval,
          image: iphone,
          title: "Parkirkan Kendaraan",
          desc: "Scan barcode di lokasi mitra parkir saat Anda ingin keluar",
        },
      ],
    },
    {
      type: "column3",
      config: {
        xs: 20,
        sm: 15,
        md: 15,
        lg: 6,
        xl: 6,
      },
      data: [
        {
          type: "card",
          no: 4,
          key: 4,
          ovalimg: oval,
          image: iphoney,
          title: "Parkirkan Kendaraan",
          desc: "Scan barcode di lokasi mitra parkir saat Anda ingin keluar",
        },
        {
          type: "card",
          no: 5,
          key: 5,
          ovalimg: oval,
          image: iphone,
          title: "Parkirkan Kendaraan",
          desc: "Scan barcode di lokasi mitra parkir saat Anda ingin keluar",
        },
      ],
    },
  ];

  const [rpActive, setrpActive] = useState(1);
  const [rpOvalActive, setRpOvalActive] = useState(oval);
  const [rpImageActive, setrpImageActive] = useState(iphone);

  return (
    <div>
      <br />
      <br />
      <br />

      {/* First */}
      <h1 className={style.headingSection} style={{ textAlign: "center" }}>
        Apa Itu Regular Parking<span className={style.yellowAsk}>?</span>
      </h1>

      <Row className={style.rowWrapperTop}>
        <Col xs={11} sm={11} md={11} lg={11} xl={11}>
          <div className={style.imgRoundedBehind}>
            <img
              className={style.imgRoundedFrontInnerBehind}
              src={roundedbottom}
              style={{ width: "100%" }}
            ></img>
          </div>
          <div className={style.imgRoundedFront}>
            <img
              className={style.imgRoundedFrontInner}
              src={roland}
              style={{ width: "100%" }}
            ></img>
          </div>
        </Col>
        <Col xs={13} sm={13} md={13} lg={13} xl={13}>
          <div className={style.wrapperRegularParkingSectionFirst}>
            <h1 className={style.regularParkingTitle}>Regular Parking</h1>
            <p className={style.regularParkingContent}>
              Solusi perparkiran motor yang memberi tahu customer dimana lokasi
              parkir terdekat. Lorem ipsum dolor set amed. Lorem ipsum dolor sit
              amet, consectetur adipiscing elit. Nullam nec sollicitudin tortor,
              et cursus turpis.{" "}
            </p>
          </div>
        </Col>
      </Row>

      {/* Second */}
      <h1 className={style.headingSection} style={{ textAlign: "center" }}>
        Mengapa Menggunakan Regular Parking
        <span className={style.yellowAsk}>?</span>
      </h1>

      <Row gutter={[0, 24]} justify="center" className={style.rowCardWhy}>
        <Col xs={10} sm={10} md={5} lg={5} xl={5} className={style.colCardWhy}>
          <Card className={style.cardWhy}>
            <div
              className={style.imageCardWhy}
              style={{
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            >
              <img src={rocket} style={{ width: "100%" }}></img>
            </div>
            <div>
              <h3 className={style.titleCardWhy}>Membership Parking</h3>
            </div>
          </Card>
        </Col>
        <Col xs={10} sm={10} md={5} lg={5} xl={5} className={style.colCardWhy}>
          <Card className={style.cardWhy}>
            <div
              className={style.imageCardWhy}
              style={{
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            >
              <img src={rocket} style={{ width: "100%" }}></img>
            </div>
            <div>
              <h3 className={style.titleCardWhy}>Diskon Menarik</h3>
            </div>
          </Card>
        </Col>
        <Col xs={10} sm={10} md={5} lg={5} xl={5} className={style.colCardWhy}>
          <Card className={style.cardWhy}>
            <div
              className={style.imageCardWhy}
              style={{
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            >
              <img src={rocket} style={{ width: "100%" }}></img>
            </div>
            <div>
              <h3 className={style.titleCardWhy}>Diskon Menarik</h3>
            </div>
          </Card>
        </Col>
        <Col xs={10} sm={10} md={5} lg={5} xl={5} className={style.colCardWhy}>
          <Card className={style.cardWhy}>
            <div
              className={style.imageCardWhy}
              style={{
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            >
              <img src={rocket} style={{ width: "100%" }}></img>
            </div>
            <div>
              <h3 className={style.titleCardWhy}>Diskon Menarik</h3>
            </div>
          </Card>
        </Col>
      </Row>

      {/* Third */}
      <h1 className={style.headingSection} style={{ textAlign: "center" }}>
        Dimana Lokasi Kami
        <span className={style.yellowAsk}>?</span>
      </h1>

      {/* Ini isi dimana lokasi kami */}

      {/* Fourth */}
      <h1 className={style.headingSection} style={{ textAlign: "center" }}>
        Bagaimana Memesan Regular Parking
        <span className={style.yellowAsk}>?</span>
      </h1>
      <Row gutter={24} justify="center">
        {regularParkingData.map((rp) => {
          return (
            <Col {...rp.config} className={style.colWrapperSectionProduct}>
              {rp.data.map((pass) => {
                if (pass.type === "card") {
                  return (
                    <Row justify="center">
                      <Card
                        bordered={false}
                        hoverable
                        className={
                          rpActive === pass.key
                            ? style.cardDeliveryActive
                            : style.cardDelivery
                        }
                        onClick={() => {
                          setrpActive(pass.key);
                          setRpOvalActive(pass.ovalimg);
                          // setrpActive(pass.ovalimg);
                          setrpImageActive(pass.image);
                        }}
                      >
                        <section className={style.ovalSection}>
                          <div className={style.ovalWrapper}>
                            <img
                              src={rpActive === pass.key ? ovalactive : oval}
                              style={{ width: "100%" }}
                            ></img>
                            <h2
                              className={
                                rpActive === pass.key
                                  ? style.ovalTitleActive
                                  : style.ovalTitle
                              }
                            >
                              {pass.no}
                            </h2>
                          </div>
                        </section>
                        <h1
                          className={
                            rpActive === pass.key
                              ? style.howOrderTitleActive
                              : style.howOrderTitle
                          }
                        >
                          {pass.title}
                        </h1>
                        <p
                          className={
                            rpActive === pass.key
                              ? style.howOrderParagraphActive
                              : style.howOrderParagraph
                          }
                        >
                          {pass.desc}
                        </p>
                      </Card>
                      {rpActive === pass.key ? (
                        <div className={style.wrapperClickImage}>
                          <img src={click} className={style.clickImage}></img>
                        </div>
                      ) : null}
                    </Row>
                  );
                } else if (pass.type === "image-handphone") {
                  return (
                    <div style={{ textAlign: "center" }}>
                      <img
                        style={{ width: "85%" }}
                        src={
                          // rpActive === pass.key
                          //   ? rpImageActive === pass.image
                          //   : rpImageActive
                          rpImageActive
                        }
                        // srcSet={
                        //   headerrp5.image.childImageSharp.fluid.srcSet
                        // }
                        // key={headerrp5.image.originalName}
                      ></img>
                    </div>
                  );
                }
              })}
            </Col>
          );
        })}
      </Row>

      {/* Fifth */}
      <h1 className={style.headingSection} style={{ textAlign: "center" }}>
        Bagaimana Cara Menjadi Mitra
        <span className={style.yellowAsk}>?</span>
      </h1>
      <div className={style.divOrder}>
        <Row justify="center" className={style.rowOrder}>
          <Col xs={22} sm={22} md={22} lg={22} xl={22} className={style.order}>
            <div className={style.wrapperOrderContent}>
              <h1 className={style.orderTitle}>Become Our Partner</h1>
              <p className={style.orderParagraph}>
                Bergabunglah dan menjadi mitra kami
              </p>
              <Button className={style.buttonJoinUs}>
                <p className={style.textButton}>Join Us</p>
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Product;
